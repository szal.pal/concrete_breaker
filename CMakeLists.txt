cmake_minimum_required(VERSION 3.7)
project(concrete_breaker)

add_definitions(
        -DFRAMES_DIR="${CMAKE_SOURCE_DIR}/data/frames"
        -DDATA_DIR="${CMAKE_SOURCE_DIR}/data/"
)

add_subdirectory(src)
add_subdirectory(test)
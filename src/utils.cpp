#include "utils.h"

#include <assert.h>
#include "boost/filesystem.hpp"

static int base_distance = -1;

namespace utils {

    float distance(cv::Point a, cv::Point b) {
        return static_cast<float>(cv::norm(cv::Mat(a), cv::Mat(b)));
    }

    cv::Point get_point(int idx, int width) {
        auto y = idx / width;
        auto x = idx - y * width;
        return cv::Point(x, y);
    }

    cv::Point calc_centroid(const std::list<cv::Point> &pt_list) {
        cv::Point ret;
        for (auto it = pt_list.begin(); it != pt_list.end(); ++it) {
            ret.x += it->x;
            ret.y += it->y;
        }
        ret.x /= pt_list.size();
        ret.y /= pt_list.size();
        return ret;
    }

    std::vector<std::string> read_directory(const std::string &dir_path, bool sort) {
        using namespace boost::filesystem;
        using namespace std;
        assert(exists(dir_path));
        vector<string> files;
        directory_iterator end_itr;
        for (directory_iterator itr(dir_path); itr != end_itr; ++itr) {
            if (!is_directory(itr->path())) {
                files.push_back(itr->path().string());
            }
        }
        if (sort)
            std::sort(files.begin(), files.end());
        return files;
    }

    int idx(const int x, const int y, const int width) {
        return y * width + x;
    }

    int get_base_distance() {
        return base_distance;
    }

    void set_base_distance(int dist) {
        base_distance = dist;
    }

}; // utils
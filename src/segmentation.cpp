#include "segmentation.h"

Segmentation::Segmentation(const cv::Mat &input) :
        input_(input.ptr()),
        img_size_(input.size()),
        parents_(static_cast<unsigned long>(input.rows * input.cols)) {
    assert(input.type() == CV_8UC1);
    assert(input.isContinuous());
    assert(
            [&]() -> bool {
                for (int i = 0; i < input.rows * input.cols; i++)
                    if (input.ptr()[i] > 1 && input.ptr()[i] != 255) {
                        std::cerr << "FOR i=" << i << " -> " << (int) input.ptr()[i] << std::endl;
                        return false;
                    }
                return true;
            }()
    ); // Is the image properly binarized?

}

void Segmentation::reset_parents() {
    for (unsigned int i = 0; i < img_size_.width * img_size_.height; i++) {
        parents_[i] = i;
    }
}


void Segmentation::check_neighbourhood(int curr, int check) {
    int sz = img_size_.width * img_size_.height;
    if (check >= 0 && check < sz && input_[check] != 0) {
        if (!are_joined(curr, check)) {
            join(check, curr);
        }
    }
}

void Segmentation::assign_parents() {
    reset_parents();

    for (int i = 0; i < img_size_.width * img_size_.height; i++) {
        if (input_[i] == 0) {
            parents_[i] = 0;
            continue;
        }

        //XXX: Silent assumption: there are no segments on the edges
        check_neighbourhood(i, i + 1);
        check_neighbourhood(i, i + img_size_.width - 1);
        check_neighbourhood(i, i + img_size_.width);
        check_neighbourhood(i, i + img_size_.width + 1);
    }
}

cv::Mat Segmentation::Perform() {
    if (segmentation_done_) return segments_;
    using namespace std;
    using namespace cv;

    assign_parents();

    segments_ = Mat::zeros(img_size_, CV_16UC1);

    // Sizes have to be equal
    assert((parents_.size()) == (img_size_.height * img_size_.width));
    assert((segments_.rows * segments_.cols) == (img_size_.height * img_size_.width));
    assert((parents_.size()) == (segments_.rows * segments_.cols));
    auto size = parents_.size();

    auto segments = reinterpret_cast<unsigned short *>(segments_.ptr());
    unsigned short curr_segment = 1;
    map<int, unsigned short> parent_to_segment;
    for (int i = 0; i < size; i++) {
        if (parents_[i] == 0) continue;
        int parent = get_parent(i);
        auto search = parent_to_segment.find(parent);
        if (search != parent_to_segment.end()) {
            segments[i] = search->second;
        } else {
            parent_to_segment[parent] = curr_segment;
            segments[i] = curr_segment++;

        }

    }

    segmentation_done_ = true;
    return segments_;
}

int Segmentation::get_parent(int idx) {
    while (parents_[idx] != idx) {
        idx = parents_[idx];
    }
    return idx;
}

bool Segmentation::are_joined(int idx_a, int idx_b) {
    return get_parent(idx_a) == get_parent(idx_b);
}

void Segmentation::join(int what, int join_to) {
    auto parent = get_parent(what);
    parents_[parent] = join_to;
}

//
// Created by develop on 27.06.17.
//

#ifndef PROJECT_SEGMENTATION_H
#define PROJECT_SEGMENTATION_H

#include <assert.h>
#include <memory>
#include "opencv2/opencv.hpp"

#include "test_utils.h"

class Segmentation {

public:
    Segmentation(const cv::Mat &input);

    cv::Mat Perform();

    Segmentation() = delete;

    Segmentation(const Segmentation &) = delete;

private:
    bool segmentation_done_ = false;
    cv::Size img_size_;

    /// Input image
    const unsigned char *input_;

    /// For implementing Union Find
    std::vector<unsigned int> parents_;

    /// Mat of shorts, where every label represents segment
    cv::Mat segments_;

    void reset_parents();

    int get_parent(int idx);

    bool are_joined(int idx_a, int idx_b);

    void join(int what, int join_to);

    void check_neighbourhood(int curr, int check);

    void assign_parents();

    FRIEND_TEST(SegmentationTest, ResetParentsTest);
    FRIEND_TEST(SegmentationTest, GetParentTest);
    FRIEND_TEST(SegmentationTest, AreJoinedTest);
    FRIEND_TEST(SegmentationTest, JoinTest);
    FRIEND_TEST(SegmentationTest, AssignParentsTest);
    FRIEND_TEST(SegmentationTest, PerformTest_Stub);
};


#endif //PROJECT_SEGMENTATION_H

//
// Created by develop on 30.06.17.
//

#ifndef PROJECT_POINT_FILTERS_H
#define PROJECT_POINT_FILTERS_H

#include "point_selector.h"

namespace filters {

    void NopeFilter(PointSelector &);

    void FilterByCertainty(PointSelector &);

    void FilterByBaseDistance(PointSelector &);

}; // filters


#endif //PROJECT_POINT_FILTERS_H

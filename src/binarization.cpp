//
// Created by develop on 29.06.17.
//

#include "binarization.h"

Binarization::Binarization(const cv::Mat &input_image, const cv::Mat &pattern) :
        input_image_(new cv::Mat(input_image)),
        pattern_(new cv::Mat(pattern)) {
    assert(input_image.type() == CV_8UC3);
    assert(pattern.type() == CV_8UC3);
    assert(input_image.data);
    assert(pattern.data);
    assert(
            pattern.size().width < input_image.size().width &&
            pattern.size().height < input_image.size().height
    );
}

cv::Mat Binarization::Perform() {
    if (binarization_performed_) return binarized_;

    using namespace cv;
    using namespace std;

    Mat img;
    cv::matchTemplate(*input_image_, *pattern_, img, CV_TM_CCOEFF_NORMED);
    cv::threshold(img, img, THRESHOLD, MAXVAL, THRESH_BINARY);
    img.convertTo(binarized_, CV_8UC1);

    binarization_performed_ = true;
    return binarized_;
}

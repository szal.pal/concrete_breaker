#include "gui/fltk_gui.h"

#include "opencv2/opencv.hpp"

#include "binarization.h"
#include "utils.h"
#include "segmentation.h"
#include "point_selector.h"
#include "points_processing.h"
#include "point_filters.h"

using namespace std;
using namespace cv;

int idx(const int &x, const int &y, const int &width, int channels = 1) {
    return channels * y * width + channels * x;
}

cv::Size min(const cv::Size &a, const cv::Size &b) {
    auto minx = a.width < b.width ? a.width : b.width;
    auto miny = a.height < b.height ? a.height : b.height;
    return cv::Size(minx, miny);
}

void resize_and_show(const Mat &_img, const std::string &msg) {
    Mat img;
    Size sz = _img.size();
//    sz /= 2;
    resize(_img, img, sz);
    imshow(msg, img);
}

cv::Mat pattern_;
cv::Point pattern_offset_(11, 12);
cv::VideoWriter video;
int i = 0;

bool assigned = false;
std::vector<cv::Point> prev_pts_;
std::vector<cv::Point> curr_pts_;

int calc_translation(cv::Point which_point, std::vector<cv::Point> to_points) {
    int curr_idx = 0;
    float curr_dist = 99999;
    for (int i = 0; i < to_points.size(); i++) {
        auto dist = utils::distance(which_point, to_points[i]);
        if (dist < curr_dist) {
            curr_idx = i;
            curr_dist = dist;
        }
    }
    return curr_idx;
}


bool ProcessImage(const cv::Mat &input_image) {

    assert(input_image.type() == CV_8UC3);

    Binarization bin(input_image, pattern_);
    auto binarized = bin.Perform();

    Segmentation segm(binarized);
    auto segmented = segm.Perform();

    PointSelector psel(segmented, pattern_offset_);
    auto pts = psel.GetPoints();

    if (!assigned) {
        assigned = true;
        prev_pts_ = pts;
        return false;
    } else {
        prev_pts_ = curr_pts_;
        curr_pts_ = pts;
    }

    Mat out(input_image);
    for (auto pt : prev_pts_) {
        circle(out, pt, 5, Scalar(10), 1);
    }
    for (auto pt : curr_pts_) {
        int cert = 1;
        try {
            cert = psel.GetCertainty(pt);
        } catch (...) {}
//        if (cert <= 5) continue;
//        cout << cert << endl;
        circle(out, pt, 3, Scalar(255 * cert / 50), -1);
        auto t = calc_translation(pt, prev_pts_);
        line(out, pt, prev_pts_[t], Scalar(0));
    }
    imshow("Output image", out);
//    video.write(out);
}

void run(std::vector<std::string> files) {
    for (auto input_file:files) {

        cv::Mat img = imread(input_file);
        assert(img.data);
        cv::Mat roi = cv::Mat(img, Rect(2007, 1223, 2350, 666));

        ProcessImage(roi);

        if (waitKey(20) == 'q') break;
    }
}

int main() {

    std::unique_ptr<Gui> gui(new FltkGui());

    gui->start();

    return 0;

    utils::set_base_distance(103);

    string files_path = DATA_DIR;
    files_path += "/example1";
    auto files = utils::read_directory(files_path);
    auto reference_file = files[0];

    string pat_path = DATA_DIR;
    pat_path += "/frames/DSC_0066_pat.jpg";
    pattern_ = imread(pat_path);

    video = VideoWriter("out_vec.avi", CV_FOURCC('M', 'J', 'P', 'G'), 10, Size(2350, 666), true);
    run(files);

}
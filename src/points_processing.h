//
// Created by develop on 29.06.17.
//

#ifndef PROJECT_POINTS_PROCESSING_H
#define PROJECT_POINTS_PROCESSING_H

#include "opencv2/opencv.hpp"
#include "data_types.h"
#include "test_utils.h"


class PointsProcessing {
    typedef PointsContainer<cv::Point> points_t;
public:
    /**
     * @param width Width of points mesh (e.g. 23x7)
     * @param height Height of points mesh (e.g. 23x7)
     */
    PointsProcessing(int width, int height);

    std::unique_ptr<points_t> GetPointTranslations(const std::vector<cv::Point> &input);

    PointsProcessing() = delete;

private:
    int width_, height_;
    bool initialized_ = false;
    std::unique_ptr<std::vector<cv::Point>> previous_points_;
    std::unique_ptr<std::vector<cv::Point>> current_points_;

    int get_nearest_neighbour(int idx) const;

    FRIEND_TEST(PointProcessingTest, GetNearestNeighbourTest);

    FRIEND_TEST(PointProcessingTest, OneCoordinateTranslationsTest);
};

#endif //PROJECT_POINTS_PROCESSING_H

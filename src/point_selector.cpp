//
// Created by develop on 28.06.17.
//

#include "point_selector.h"
#include <assert.h>
#include <vector>
#include <list>

#include "opencv2/opencv.hpp"
#include "utils.h"

PointSelector::PointSelector(const cv::Mat &segmented_image, const cv::Point &pat_offset) :
        segmented_image_(new cv::Mat(segmented_image)),
        offset_(new cv::Point(pat_offset)),
        points_selected_(false) {
    assert(segmented_image.type() == CV_16UC1);
    assert(segmented_image.isContinuous());
    assert(
            [&segmented_image]() -> bool {
                double dmin;
                double dmax;
                minMaxLoc(segmented_image, &dmin, &dmax);
                int min = static_cast<int>(dmin);
                int max = static_cast<int>(dmax);
                if (min != 0) return false;
                auto ptr = reinterpret_cast<const unsigned short *>(segmented_image.ptr());
                int curr = 1;
                for (int i = 0; i < segmented_image.rows * segmented_image.cols; i++) {
                    if (ptr[i] == curr) {
                        if (ptr[i] == max) return true;
                        curr++;
                        i = 0;
                        continue;
                    }
                }
                return false;
            }()
    ); // Is the image properly segmented?

    double _min, _max;
    cv::minMaxLoc(*segmented_image_, &_min, &_max);
    max_segment_ = static_cast<int>(_max);
}

std::vector<cv::Point> PointSelector::GetPoints() {
    using namespace std;
    using namespace cv;

    if (points_selected_) return points_;

    points_.clear();

    create_segment_data_structure();

    for (int i = 1; i < segments_.size(); i++) { // i=1, cause i==0 -> background
        points_.push_back(utils::calc_centroid(segments_[i]) + *offset_);
    }

    for (auto filter:filters_) {
        filter(*this);
    }

    points_selected_ = true;
    return points_;
}

void PointSelector::create_segment_data_structure() {
    assert(max_segment_ > 0);

    segments_ = std::vector<std::list<cv::Point>>(static_cast<unsigned int>(max_segment_ + 1));

    auto psegm = reinterpret_cast<const unsigned short *>(segmented_image_->ptr());

    for (int i = 0; i < segmented_image_->rows * segmented_image_->cols; i++) {
        segments_[psegm[i]].push_back(utils::get_point(i, segmented_image_->cols));
    }
}

int PointSelector::GetCertainty(cv::Point point) const {

    assert(points_selected_);
    point -= *offset_;
    for (int i = 1; i < segments_.size(); ++i) { // i=1, because i==0 is background
        for (auto pt:segments_[i]) {
            if (pt == point) {
                return static_cast<int>(segments_[i].size());
            }
        }
    }
    std::stringstream errmsg;
    errmsg << "Given point (" << point.x << ", " << point.y << ") not found amongst segments";
    throw std::logic_error(errmsg.str());
}

void PointSelector::AddFilter(filter_t filter) {
    filters_.push_back(filter);
    points_selected_ = false; // If filters has beed changed, point are outdated
}


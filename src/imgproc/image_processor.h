//
// Created by szal on 22.07.17.
//

#ifndef CONCRETE_BREAKER_IMAGE_PROCESSOR_H
#define CONCRETE_BREAKER_IMAGE_PROCESSOR_H

#include <vector>
#include "opencv2/core.hpp"

class ImageProcessor {
public:
    /**
     * Process image and return tranlsations of particular points
     */
    std::vector <cv::Point> process(cv::Mat frame) = 0;

    virtual ~ImageProcessor() {}
};

#endif //CONCRETE_BREAKER_IMAGE_PROCESSOR_H

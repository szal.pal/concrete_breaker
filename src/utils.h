#ifndef PROJECT_UTILS_H
#define PROJECT_UTILS_H

#include "opencv2/opencv.hpp"

namespace utils {

    float distance(cv::Point a, cv::Point b);

    cv::Point get_point(int idx, int width);

    cv::Point calc_centroid(const std::list<cv::Point> &pt_list);

    int idx(const int x, const int y, const int width);

    /**
     * Read all files contained in directory
     */
    std::vector<std::string> read_directory(const std::string &dir_path, bool sort = true);

    int get_base_distance();

    void set_base_distance(int dist);

}; // utils

#endif //PROJECT_UTILS_H

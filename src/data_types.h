//
// Created by develop on 29.06.17.
//

#ifndef PROJECT_DATA_TYPES_H
#define PROJECT_DATA_TYPES_H

//class cv::Point;

template<class T>
class PointsContainer {
public:

    PointsContainer(int width, int height, T val) {
        width_ = width;
        height_ = height;
        points_ = std::vector<std::vector<T>>(width, std::vector<T>(height, val));
        size_ = 0;
        for (int i = 0; i < points_.size(); i++) size_ += points_[i].size();
    }

    const std::vector<T>& operator[](int idx) const {
        return points_[idx];
    }

    std::vector<T>& operator[](int idx) {
        return points_[idx];
    }

    int size() const {
        return size_;
    }

    PointsContainer() = delete;

    int width_, height_, size_;
    std::vector<std::vector<T>> points_;

};

#endif //PROJECT_DATA_TYPES_H

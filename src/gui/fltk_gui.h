//
// Created by szal on 22.07.17.
//

#ifndef CONCRETE_BREAKER_FLTK_GUI_H
#define CONCRETE_BREAKER_FLTK_GUI_H

#include "gui.h"

class FltkGui : public Gui {
public:
    void start() override;

};

#endif //CONCRETE_BREAKER_FLTK_GUI_H

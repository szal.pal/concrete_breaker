//
// Created by szal on 22.07.17.
//

#ifndef CONCRETE_BREAKER_GUI_H
#define CONCRETE_BREAKER_GUI_H

class Gui {
public:
    virtual void start() = 0;

    virtual ~Gui() {}
};

#endif //CONCRETE_BREAKER_GUI_H

//
// Created by develop on 29.06.17.
//

#ifndef PROJECT_BINARIZATION_H
#define PROJECT_BINARIZATION_H

#include <memory>
#include "opencv2/opencv.hpp"

class Binarization {

public:
    Binarization(const cv::Mat &input_image, const cv::Mat &pattern);

    cv::Mat Perform();

    Binarization() = delete;

    Binarization(const Binarization &) = delete;

private:
    const float THRESHOLD = 0.5;
    const float MAXVAL=255;
    std::unique_ptr<const cv::Mat> input_image_, pattern_;
    bool binarization_performed_=false;
    cv::Mat binarized_;
};


#endif //PROJECT_BINARIZATION_H

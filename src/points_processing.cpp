#include "points_processing.h"
#include "utils.h"

PointsProcessing::PointsProcessing(int width, int height) :
        width_(width),
        height_(height) {
    assert(width > 0 && height > 0);
}

std::unique_ptr<PointsProcessing::points_t>
PointsProcessing::GetPointTranslations(const std::vector<cv::Point> &input) {
    using namespace std;
    using namespace cv;
    if (!initialized_) {
        current_points_ = unique_ptr<vector<Point>>(new vector<Point>(input));
        initialized_ = true;
        return unique_ptr<points_t>(new points_t(width_, height_, Point(0, 0)));
    }
    previous_points_ = move(current_points_);
    current_points_ = unique_ptr<vector<Point>>(new vector<Point>(input));
    unique_ptr<points_t> output_points(new points_t(width_, height_, Point(0, 0)));

    points_t &pts = *output_points; // just a little alias
    for (int x = 0; x < width_; x++) {
        for (int y = 0; y < height_; y++) {
            int curr_idx = utils::idx(x, y, width_);
            int prev_idx = get_nearest_neighbour(curr_idx);
            Point curr_pt = (*current_points_)[curr_idx];
            Point prev_pt = (*previous_points_)[prev_idx];
            pts[x][y] = curr_pt-prev_pt;
        }
    }

    return output_points;
}

int PointsProcessing::get_nearest_neighbour(int idx) const {
    assert(current_points_->size() > idx);
    auto point = (*current_points_)[idx];

    // For nearest pixel
    int curr_idx = -1;
    float curr_distance = width_ * height_;

    for (int i = 0; i < previous_points_->size(); i++) {
        auto _pt = (*previous_points_)[i];
        float dist = utils::distance(point, _pt);
        if (curr_distance > dist) {
            curr_idx = i;
            curr_distance = dist;
        }
    }

    return curr_idx;
}

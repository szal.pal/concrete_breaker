//
// Created by develop on 28.06.17.
//

#ifndef PROJECT_POINT_SELECTOR_H
#define PROJECT_POINT_SELECTOR_H

#include <vector>
#include <functional>
#include "opencv2/opencv.hpp"
#include "test_utils.h"

//TODO ENSURE PROPER (CONSTANT) POINTS LAYOUT
//TODO REMEMBER ABOUT MISSING POINTS

class PointSelector {

public:
    using filter_t = std::function<void(PointSelector&)>;
    /**
     * @param segmented_image Must be type CV_16UC1
     * @param pat_offset Offset between pattern location match and actual point
     */
    PointSelector(const cv::Mat &segmented_image, const cv::Point &pat_offset);

    /**
     * Based on segmented image, get coordinates of analysed points
     */
    std::vector<cv::Point> GetPoints();

    void AddFilter(filter_t filter);

    /**
     * Compute **certainty** of given point
     *
     * The computing method counts number of pattern matches for given point.
     * The idea is simple: the more matches, the more certain is the point.
     *
     * Possible values are around: [0 .. 600]
     */
    int GetCertainty(cv::Point point) const;

    PointSelector() = delete;

    PointSelector(const PointSelector &) = delete;


private:
    bool points_selected_;
    std::vector<cv::Point> points_;
    int max_segment_; /// Greatest label value of segment
    std::unique_ptr<const cv::Mat> segmented_image_;
    std::vector<std::list<cv::Point>> segments_;
    std::unique_ptr<cv::Point> offset_;

    std::vector<filter_t> filters_;

    void create_segment_data_structure();

    friend bool ProcessImage(const cv::Mat &input_image);  //temp

    FRIEND_TEST(PointSelectorTest, CreateSegmentDataStructureTest);
    FRIEND_TEST(PointSelectorTest, GetCertaintyTest);
    FRIEND_TEST(PointSelectorTest, FiltersTest);
};

#endif //PROJECT_POINT_SELECTOR_H

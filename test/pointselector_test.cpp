#include <point_selector.h>
#include "gtest/gtest.h"
#include "opencv2/opencv.hpp"
#include "utils.h"
#include "segmentation.h"

using namespace cv;
using namespace std;

namespace {
    string binary_small_filename = "/DSC_0066_binary_small.tif";
    string binary_filename = "/DSC_0066_binary.tif";
    Mat binary_small_img_, binary_img_;
    Point pat_offset = Point(11, 12);

    // 1 2 3
    // 4 5 6
    vector<Point> ref_pts_small_ = {{54,  41},
                                    {157, 48},
                                    {264, 48},
                                    {52,  144},
                                    {158, 147},
                                    {267, 155}};
    int error_ = 5; // [px]

    class PointSelectorTest : public ::testing::Test {
    protected:

        static void SetUpTestCase() {
            set_small_img();
            set_img();
        }

    private:
        static void set_small_img() {
            string bin_path = FRAMES_DIR;
            bin_path += binary_small_filename;
            binary_small_img_ = imread(bin_path);
            ASSERT_TRUE(binary_small_img_.data) << "Failed to initialize cv::Mat";
            cvtColor(binary_small_img_, binary_small_img_, CV_RGB2GRAY);
        }

        static void set_img() {
            string bin_path = FRAMES_DIR;
            bin_path += binary_small_filename;
            binary_img_ = imread(bin_path);
            ASSERT_TRUE(binary_img_.data) << "Failed to initialize cv::Mat";
            cvtColor(binary_img_, binary_img_, CV_RGB2GRAY);
        }
    };

    void verify_segments_data_structure(const std::list<cv::Point> &check, const vector<int> &ref, int width) {
        auto it = check.begin();
        ASSERT_EQ(ref.size(), check.size()) << "Sizes do not match";
        for (auto r :ref) {
            EXPECT_EQ(r, it->y * width + it->x);
            ++it;
        }
    }

} // namespace


TEST_F(PointSelectorTest, CreateSegmentDataStructureTest) {
    auto stub = new unsigned short[91];
    for (int i = 0; i < 91; i++) stub[i] = 0;

    vector<int> segm_1 = {3, 15, 17, 28, 29, 30, 43};
    vector<int> segm_2 = {23, 24, 37, 47, 48, 50, 60, 61, 63, 75};
    vector<int> segm_3 = {66, 67, 68, 69};
    vector<int> segm_4 = {71};

#define FILL_STUB(nr)               \
    for (auto s : segm_##nr) {      \
        stub[s]=nr;                 \
    }

    FILL_STUB(1);
    FILL_STUB(2);
    FILL_STUB(3);
    FILL_STUB(4);
#undef FILL_STUB

    auto segments = Mat(7, 13, CV_16UC1, stub);
    PointSelector sel(segments, pat_offset);

    sel.create_segment_data_structure();

    // 5 -> 4 segments and background
    ASSERT_EQ(5, sel.segments_.size()) << "Something gone terribly wrong";

    verify_segments_data_structure(sel.segments_[1], segm_1, segments.cols);
    verify_segments_data_structure(sel.segments_[2], segm_2, segments.cols);
    verify_segments_data_structure(sel.segments_[3], segm_3, segments.cols);
    verify_segments_data_structure(sel.segments_[4], segm_4, segments.cols);

}


TEST_F(PointSelectorTest, PointSelectorSmallTest) {
    Segmentation s(binary_small_img_);
    PointSelector sel(s.Perform(), pat_offset);
    auto pts = sel.GetPoints();
    ASSERT_EQ(ref_pts_small_.size(), pts.size()) << "Sizes doesn't match";

    for (int i = 0; i < pts.size(); i++) {
        cout << ref_pts_small_[i] << "\t" << pts[i] << endl;
        EXPECT_LE(utils::distance(ref_pts_small_[i], pts[i]), error_);
    }
}

TEST_F(PointSelectorTest, FiltersTest) {
    Segmentation s(binary_small_img_);
    PointSelector sel(s.Perform(), pat_offset);

    auto pts = sel.GetPoints();
    pts.push_back({-1, -1});

    sel.AddFilter([](PointSelector &psel) -> void { psel.points_.push_back({-1, -1}); });
    auto pts2 = sel.GetPoints();

    ASSERT_EQ(pts.size(), pts2.size());
    for (int i = 0; i < pts.size(); i++) {
        ASSERT_EQ(pts[i], pts2[i]);
    }
}

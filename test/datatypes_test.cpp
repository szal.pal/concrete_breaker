#include "gtest/gtest.h"
#include "data_types.h"

TEST(DataTypesTest, PointsContainerCreatingTest) {
    int w = 10, h = 5, v = 666;
    PointsContainer<int> pts(w, h, v);
    ASSERT_EQ(w, pts.points_.size());
    ASSERT_EQ(h, pts.points_[0].size());
    for (int i = 0; i < w; i++) {
        for (int j = 0; j < h; j++) {
            ASSERT_EQ(v, pts.points_[i][j]);
        }
    }
}

TEST(DataTypesTest, OperatorTest) {
    int w=10,h=5,v=0;
    PointsContainer<int> pts(w,h,v);
    pts.points_[6][3] = 666;
    ASSERT_EQ(666,pts[6][3]);
}
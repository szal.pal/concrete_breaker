#include "points_processing.h"
#include "gtest/gtest.h"
#include "opencv2/opencv.hpp"
#include "utils.h"

using namespace cv;
using namespace std;

TEST(PointProcessingTest, GetNearestNeighbourTest) {
    vector<Point> points = {Point(2, 1), Point(7, 1), Point(4, 4), Point(7, 2)};
    vector<Point> check = {Point(3, 1), Point(12, 1)};
    vector<Point> ref = {Point(2, 1), Point(7, 1)};
    int w = 14, h = 6;
    PointsProcessing proc(w, h);
    proc.previous_points_ = make_unique<vector<Point>>(points);
    proc.current_points_ = make_unique<vector<Point>>(check);
    auto cp = *(proc.previous_points_);
    for (int i = 0; i < ref.size(); i++) {
        auto v = cp[proc.get_nearest_neighbour(i)];
        ASSERT_EQ(ref[i], v);
    }
}

TEST(PointProcessingTest, OnlyZerosTest) {
    vector<Point> pts_in;
    int w = 23, h = 5;
    for (int i = 0; i < w * h; i++) {
        pts_in.push_back(Point(0, 0));
    }
    PointsProcessing pts_proc(w, h);

    for (int i = 0; i < 10; i++) {
        auto pts = pts_proc.GetPointTranslations(pts_in);
        for (int x = 0; x < pts->width_; x++) {
            for (int y = 0; y < pts->height_; y++) {
                ASSERT_EQ(Point(0,0), pts->points_[x][y]);
            }
        }
    }
}

TEST(PointProcessingTest, OneCoordinateTranslationsTest) {
    vector<Point> pts_prev = {Point(2, 0), Point(7, 1), Point(11, 2), Point(2, 3), Point(7, 4), Point(11, 5)};
    vector<Point> pts_curr = {Point(2, 1), Point(7, 2), Point(11, 3), Point(2, 4), Point(7, 5), Point(11, 6)};
    int w = 3, h = 2;
    PointsProcessing::points_t ref_translation(w, h, Point(0, 0));
    ASSERT_EQ(w * h, ref_translation.size()) << "Sizes do not match";
    for (int x = 0; x < ref_translation.width_; x++) {
        for (int y = 0; y < ref_translation.height_; y++) {
            ref_translation.points_[x][y] = Point(0,1);
        }
    }

    PointsProcessing pts_proc(w,h);
    auto pts = pts_proc.GetPointTranslations(pts_prev);

    auto check_result = [&](const Point& expected_result) -> void {
        for(int x=0;x<w;x++) {
            for (int y=0;y<h;y++) {
                ASSERT_EQ(expected_result, (*pts)[x][y]) << "Unexpected result";
            }
        }
    };

    check_result(Point(0,0));

    pts = pts_proc.GetPointTranslations(pts_curr);

    check_result(Point(0,1));

}


#include "gtest/gtest.h"
#include "opencv2/opencv.hpp"
#include "utils.h"

using namespace cv;

TEST(UtilsTest, GetIdxTest) {
    int width = 13;
    EXPECT_EQ(Point(0, 0), utils::get_point(0, width));
    EXPECT_EQ(Point(2, 1), utils::get_point(15, width));
    EXPECT_EQ(Point(0, 2), utils::get_point(26, width));
    EXPECT_EQ(Point(3, 0), utils::get_point(3, width));
    EXPECT_EQ(Point(12, 6), utils::get_point(90, width));
}

TEST(UtilsTest, CalcCentroidTest) {
    Point p1(5,2);
    Point p2(7,9);
    Point p3(34,1);
    Point p4(45,8);
    Point p5(9,4);
    Point p6(2,5);

    std::list<Point> l = {p1,p2,p3,p4,p5,p6};
    auto pt = utils::calc_centroid(l);
    ASSERT_EQ(pt, Point(17, 4));
}

TEST(UtilsTest, ReadDirectoryTest) {
    using namespace std;
    string path = FRAMES_DIR;
    path += "/../test_dir";
    auto files = utils::read_directory(path);

    EXPECT_STREQ("test1", files[0].substr(files[0].find_last_of('/')+1).c_str());
    EXPECT_STREQ("test2", files[1].substr(files[1].find_last_of('/')+1).c_str());
    EXPECT_STREQ("test3", files[2].substr(files[2].find_last_of('/')+1).c_str());
}
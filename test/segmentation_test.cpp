#include "gtest/gtest.h"
#include "segmentation.h"

#include <memory>

using namespace std;
using namespace cv;


namespace {
    string binary_filename = "/DSC_0066_binary.tif";

    class SegmentationTest : public ::testing::Test {
    protected:
        unique_ptr<Segmentation> s;

        virtual void SetUp() {
            string bin_path = FRAMES_DIR;
            bin_path += binary_filename;
            Mat bin_img = imread(bin_path);
            ASSERT_TRUE(bin_img.data) << "Failed to initialize cv::Mat";
            cvtColor(bin_img, bin_img, CV_RGB2GRAY, 1);
            s = make_unique<Segmentation>(bin_img);
        }

        virtual ~SegmentationTest() {}

        void replace_segmentation(unsigned char *data, int width, int height) {
            Mat input_stub = Mat(height, width, CV_8UC1, data);
            s = make_unique<Segmentation>(input_stub);
        }

        void view_segments(const Mat &seg) {
            assert(seg.type() == CV_16UC1);
            Mat r = Mat::zeros(seg.size(), CV_8UC1);
            Mat g = Mat::zeros(seg.size(), CV_8UC1);
            Mat b = Mat::zeros(seg.size(), CV_8UC1);

            auto pseg = reinterpret_cast<const unsigned short *>(seg.ptr());
            auto pr = r.ptr();
            auto pg = g.ptr();
            auto pb = b.ptr();

            for (int i = 0; i < seg.rows * seg.cols; i++) {
                if (pseg[i] != 0) {
                    auto color = pseg[i] % 3;
                    if (color == 0) pr[i] = 255;
                    if (color == 1) pg[i] = 255;
                    if (color == 2) pb[i] = 255;
                } else {
                    pr[i] = pg[i] = pb[i] = 255;
                }
            }

            vector<Mat> _img = {r, g, b};
            Mat img;
            merge(_img, img);
            imshow("Segmentation result", img);

//            Mat img;
//            seg.convertTo(img, CV_8UC1);
//            imshow("Segmentation result", img);

            waitKey(0);
        }
    };


} // namespace

TEST_F(SegmentationTest, ResetParentsTest) {

    auto sz = s->img_size_.width * s->img_size_.height;
    for (unsigned int i = 0; i < sz; i++) {
        s->parents_[i] = 0;
    }
    s->reset_parents();
    for (unsigned int i = 0; i < sz; i++) {
        ASSERT_EQ(i, s->parents_[i]);
    }
}

// Array: {0, 1, 2, 3, 4, 5}
// Joins: 4->3, 3->2, 2->1
TEST_F(SegmentationTest, GetParentTest) {
    vector<unsigned int> par = {0, 1, 1, 2, 3, 5};
    int child = 4, parent = 1;
    s->parents_ = std::move(par);
    for (int i = 0; i < par.size(); i++) {
        ASSERT_EQ(par[i], s->parents_[i]) << "Incorrect parents_ assignment";
    }
    ASSERT_EQ(parent, s->get_parent(child));
}

TEST_F(SegmentationTest, AreJoinedTest) {
    vector<unsigned int> par = {0, 1, 1, 2, 3, 5};
    int a = 4, b = 2;
    s->parents_ = std::move(par);
    for (int i = 0; i < par.size(); i++) {
        ASSERT_EQ(par[i], s->parents_[i]) << "Incorrect parents_ assignment";
    }
    ASSERT_TRUE(s->are_joined(a, b));
}

TEST_F(SegmentationTest, JoinTest) {
    vector<unsigned int> par = {0, 1, 2, 3, 4, 5};
    int what = 4, to = 3;
    s->parents_ = std::move(par);
    for (int i = 0; i < par.size(); i++) {
        ASSERT_EQ(par[i], s->parents_[i]) << "Incorrect parents_ assignment";
    }
    EXPECT_FALSE(s->are_joined(what, to));
    s->join(what, to);
    EXPECT_TRUE(s->are_joined(what, to));
}

TEST_F(SegmentationTest, AssignParentsTest) {

    auto stub = new unsigned char[90];
    for (int i = 0; i < 91; i++) stub[i] = 0;
    vector<int> stub_idx = {3, 15, 17, 23, 24, 28, 29, 30, 37, 43, 47, 48, 50, 60, 61, 63, 66, 67, 68, 69, 71, 75};
    for (auto i:stub_idx) {
        stub[i] = 1;
    }
    s->input_ = stub;
    s->img_size_ = Size(13, 7);

    s->assign_parents();

    vector<int> par_3 = {3, 15, 17, 28, 29, 30, 43};
    vector<int> par_23 = {24, 47, 48, 60, 61, 23, 37, 50, 63, 75};
    vector<int> par_66 = {66, 67, 68, 69};
    vector<int> par_71 = {71};

    ASSERT_TRUE(stub_idx.size() == par_3.size() + par_23.size() + par_66.size() + par_71.size())
                                << "Something wrong in implementation";

    for (auto i:par_3) {
        EXPECT_EQ(3, s->get_parent(i));
    }
    for (auto i:par_23) {
        EXPECT_EQ(23, s->get_parent(i));
    }
    for (auto i:par_66) {
        EXPECT_EQ(66, s->get_parent(i));
    }
    for (auto i:par_71) {
        EXPECT_EQ(71, s->get_parent(i));
    }
}

TEST_F(SegmentationTest, PerformTest_Stub) {

    auto stub = new unsigned char[91];
    for (int i = 0; i < 91; i++) stub[i] = 0;
    vector<int> stub_idx = {3, 15, 17, 23, 24, 28, 29, 30, 37, 43, 47, 48, 50, 60, 61, 63, 66, 67, 68, 69, 71, 75};
    for (auto i:stub_idx) {
        stub[i] = 1;
    }
    replace_segmentation(stub, 13, 7);

    Mat segments = s->Perform();

    vector<int> segm_1 = {3, 15, 17, 28, 29, 30, 43};
    vector<int> segm_2 = {24, 47, 48, 60, 61, 23, 37, 50, 63, 75};
    vector<int> segm_3 = {66, 67, 68, 69};
    vector<int> segm_4 = {71};
    vector<int> segms(s->img_size_.width * s->img_size_.height);
    for (auto v:segm_1) segms[v] = 1;
    for (auto v:segm_2) segms[v] = 2;
    for (auto v:segm_3) segms[v] = 3;
    for (auto v:segm_4) segms[v] = 4;

    ASSERT_TRUE(stub_idx.size() == segm_1.size() + segm_2.size() + segm_3.size() + segm_4.size())
                                << "Something wrong in implementation";

    auto p_result = reinterpret_cast<unsigned short *>(segments.ptr());
    for (int i = 0; i < s->img_size_.width * s->img_size_.height; i++) {
        EXPECT_EQ(segms[i], p_result[i]);
    }

}

TEST_F(SegmentationTest, PerformTest) {
    Mat segments = s->Perform();
    std::cout << "\nTEST_F(SegmentationTest, PerformTest) -> THIS TEST IS FOR VISUALIZATION (but it is commented...)\n";
//    view_segments(segments);
    ASSERT_TRUE(segments.data);
}